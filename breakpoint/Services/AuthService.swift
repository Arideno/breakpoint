//
//  AuthService.swift
//  breakpoint
//
//  Created by Andrew Moisol on 7/23/18.
//  Copyright © 2018 Andrew Moisol. All rights reserved.
//

import Foundation
import FirebaseAuth

class AuthService {
    static let instance = AuthService()
    
    func registerUser(withEmail email: String, andPassword password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            guard let user = authResult?.user else {
                userCreationComplete(false, error)
                return
            }
            
            let userData = ["provider": user.providerID, "email": user.email!] as Dictionary<String, Any>
            DataService.instance.createDBUser(uid: user.uid, userData: userData)
            userCreationComplete(true, nil)
        }
    }
    
    func loginUser(withEmail email: String, andPassword password: String, loginComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if error != nil {
                loginComplete(false, error)
                return
            }
            loginComplete(true, nil)
        }
    }
    
    func loginUserWith(credentials: AuthCredential, loginComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().signInAndRetrieveData(with: credentials) { (authResult, error) in
            guard let user = authResult?.user else {
                loginComplete(false, error)
                return
            }
            
            let userData = ["provider": user.providerID, "email": user.email!] as Dictionary<String, Any>
            DataService.instance.createDBUser(uid: user.uid, userData: userData)
            loginComplete(true, nil)
        }
    }
}
