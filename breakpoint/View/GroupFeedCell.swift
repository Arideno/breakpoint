//
//  GroupFeedCell.swift
//  breakpoint
//
//  Created by Andrew Moisol on 7/24/18.
//  Copyright © 2018 Andrew Moisol. All rights reserved.
//

import UIKit

class GroupFeedCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    
    func configureCell(profileImage: UIImage, email: String, content: String) {
        self.profileImageView.image = profileImage
        self.emailLbl.text = email
        self.contentLbl.text = content
    }

}
