//
//  UserCell.swift
//  breakpoint
//
//  Created by Andrew Moisol on 7/24/18.
//  Copyright © 2018 Andrew Moisol. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    var showing = false
    
    func configureCell(profileImage image: UIImage, email: String, isSelected: Bool) {
        self.profileImageView.image = image
        self.emailLbl.text = email
        if isSelected {
            checkImageView.isHidden = false
        } else {
            checkImageView.isHidden = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            if showing == false {
                checkImageView.isHidden = false
                showing = true
            } else {
                checkImageView.isHidden = true
                showing = false
            }
        }
    }

}
