//
//  UIViewExt.swift
//  breakpoint
//
//  Created by Andrew Moisol on 7/23/18.
//  Copyright © 2018 Andrew Moisol. All rights reserved.
//

import UIKit

extension UIView {
    func bindToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification: Notification) {
        
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let beginningFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! CGRect
        let endFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect
        let deltaY = endFrame.origin.y - beginningFrame.origin.y
        
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
    }
}
